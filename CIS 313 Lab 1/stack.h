//Eric Schultz
//Implementation of stack ADT

#include <stdio.h>
#include <stdlib.h>

#define MAX_STACK_SIZE 100

//Structure that houses each character entered.
typedef struct stackElt {
    char c;
    struct stackElt* link;
}SELT;
//Structure that keeps track of entire stack.
typedef struct {
    int numElems;
    struct stackElt* top;
}STACK;

STACK* createStack();
void push(STACK* stack, char toBeAdded);
char pop(STACK* stack);
int s_isEmpty(STACK* stack);
void destroyStack(STACK* stack);
//Creates stack and allocates memory for the data fields within.
STACK* createStack() {

    STACK* newStack;
    newStack = (STACK*)malloc(sizeof(STACK));

    newStack->numElems = 0;
    newStack->top = NULL;

    return newStack;
}
//Pushes an element onto the stack which houses the character input.
void push(STACK* stack, char toBeAdded) {

    SELT* newElt;
    newElt = (SELT*)malloc(sizeof(SELT));
    newElt->c = toBeAdded;

    if(stack->numElems == 0) {
        newElt->link = NULL;
    } else {
        newElt->link = stack->top;
    }

    stack->top = newElt;
    ++(stack->numElems);

}
//Pops an element off the stack and frees the element from memory.
char pop(STACK* stack) {

    char toBeRemoved;
    SELT* temp;

    if(stack->numElems == 0) {
        return '\0';
    } else {
        temp = stack->top;
        toBeRemoved = stack->top->c;
        stack->top = stack->top->link;
        free(temp);
        (stack->numElems)--;
    }
    return toBeRemoved;
}
//Simply returns whether or not the stack has 0 elements in it.
int s_isEmpty(STACK* stack) {

    if(stack->numElems == 0) {
        return 1;
    } else {
        return 0;
    }
}
//Frees all memory in use by the stack (if any).
void destroyStack(STACK* stack) {

    SELT* toBeDeleted;

    while(stack->top != NULL) {
        toBeDeleted = stack->top;
        stack->top = stack->top->link;
        free(toBeDeleted);
    }

    free( stack );

}

