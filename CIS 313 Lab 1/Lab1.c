//Eric Schultz
//CIS313 Lab 1
//Palindromes

#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include "stack.h"
#include "queue.h"

#define MAX_CHARS_PER_STRING 50
#define MAX_STRINGS 10

int input(char [][MAX_CHARS_PER_STRING]);
void isPalindrome(char string[][MAX_CHARS_PER_STRING], int strnum);

int main(void) {

    char str[MAX_STRINGS][MAX_CHARS_PER_STRING] = { NULL };
    int strnum;

    strnum = input(str); //filling 2d array with strings

    isPalindrome(str, strnum);

    return 0;
}
/* input: takes a 2d array of chars that are entered by the user,
    and returns the number of strings entered .*/
int input(char str[][MAX_CHARS_PER_STRING]) {

    int n;
    printf("Enter number of strings to be tested, followed by the strings themselves: ");
    scanf("%d", &n);

    for(int i = 0; i < n; i++) {
        scanf(" %[^\n]", &(str[i]));
    }

    return n;
}
/* isPalindrome: takes the array of strings and the number of strings,
    then prints whether or not each string is a palindrome. */
void isPalindrome(char str[][MAX_CHARS_PER_STRING], int strnum) {

    int i = 0;
    char temp;
    char* token;
    char* specChars = ",.;\'[]!@#$%^&\"*()_+-={}:\\<>\? ";

    while(i < strnum) { //I realize I should have put this loop in main(), I would have an O(n) isPalindrome() if I had :/

        int j = 0, k = 0, l = 0, tempint = 0;
        char string1[MAX_CHARS_PER_STRING] = { NULL };
        char string2[MAX_CHARS_PER_STRING] = { NULL };
        QUEUE* stringQ = createQueue();
        STACK* stringS = createStack();

        token = strtok(str[i], specChars);

        //Here we parse the input strings so we can check for palindrome-ness later
        while(token != NULL) {
            strcat(string1, token);
            token = strtok(NULL, specChars);
        }
        tempint = strlen(string1);
        while(l < tempint) {
            string1[l] = tolower(string1[l]);
            l++;
        }

        token = NULL;

        //Push every character of the string to the stack
        while(string1[j] != '\0') {
            push(stringS, string1[j]);
            j++;
        }

        //Pop then enqueue every character, getting the reverse string
        while(s_isEmpty(stringS) == 0) {
            temp = pop(stringS);
            enqueue(stringQ, temp);
        }

        //Copy each char to a new string
        while(q_isEmpty(stringQ) == 0) {
            string2[k] = dequeue(stringQ);
            k++;
        }

        //Finally, compare strings to determine if palindrome or not
        if(strcmp(string1, string2) == 0) {
            printf("\nThis is a palindrome");
        } else {
            printf("\nThis is not a palindrome");
        }
        //I swear I'll learn to use stand i/o for this next time! I need to research how to do it in C...

        //Free all created memory
        destroyQueue(stringQ);
        destroyStack(stringS);

        i++;
    }

    return;
}


