//Eric Schultz
//Implementation of queue ADT
//I realize this is a doubly linked list instead of single, I hope that's okay?

#include <stdio.h>
#include <stdlib.h>
//Structure that acts as an element of the queue; holds a single character.
typedef struct queueElt {
    struct queueElt* next;
    struct queueElt* previous;
    char c;
}QELT;
//Structure that keeps track of entire queue data structure.
typedef struct {
    int numElems;
    struct queueElt* front;
    struct queueElt* back;
}QUEUE;

QUEUE* createQueue();
void enqueue(QUEUE* queue, char toBeAdded);
char dequeue(QUEUE* queue);
int q_isEmpty(QUEUE* queue);
//Creates queue and allocates memory for it.
QUEUE* createQueue() {

    QUEUE* newQueue;
    newQueue = (QUEUE*)calloc(1, sizeof(QUEUE));

    (newQueue->numElems) = 0;
    newQueue->front = NULL;
    newQueue->back = NULL;

    return newQueue;
}
//Enqueues an element to the back of the queue.
void enqueue(QUEUE* queue, char toBeAdded) {

    QELT* newElt;
    newElt = (QELT*)calloc(1, sizeof(QELT));
    newElt->c = toBeAdded;

    if(queue->numElems == 0) {
        queue->front = newElt;
        queue->back = newElt;
        newElt->next = NULL;
        newElt->previous = NULL;
        (queue->numElems)++;
    } else {
        ++(queue->numElems);
        newElt->next = queue->back;
        queue->back->previous = newElt;
        newElt->previous = NULL;
        queue->back = newElt;
    }

}
//Removes the element at the front of the queue and sets the element behind that to the new front.
char dequeue(QUEUE* queue) {

    char toBeRemoved;
    QELT* temp;

    if(queue->numElems == 0) {
        return '\0';
    } else {
        toBeRemoved = queue->front->c;
        temp = queue->front;

        if(queue->numElems == 1) {
            queue->front = queue->back = NULL;
        } else {
            queue->front = queue->front->previous;
        }

        free(temp);
        --(queue->numElems);

    }
    return toBeRemoved;
}
//Simply tells whether or not the queue has any elements in it.
int q_isEmpty(QUEUE* queue) {

    if(queue->numElems == 0) {
        return 1;
    } else {
        return 0;
    }
}
//Frees all memory used by the queue data structure.
void destroyQueue(QUEUE* queue) {

    QELT* toBeDeleted;

    while(queue->front != NULL) {
        toBeDeleted = queue->front;
        queue->front = queue->front->previous;
        free(toBeDeleted);
    }
    free(queue);

}

