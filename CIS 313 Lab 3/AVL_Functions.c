
//Eric Schultz
//11/7/2014
//File with functions for binary search tree

#include <ctype.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "AVLADT.h"

//AVL: Prototype Declarations for private functions
static NODE* _insert (AVL_TREE* tree,
					  NODE* root,
					  NODE* newPtr);
static NODE* _delete (AVL_TREE* tree,
					  NODE* root,
					  void* dataPtr,
					  bool* success);
static void* _retrieve(AVL_TREE* tree,
					   void* dataPtr,
					   NODE* root);
static void _destroy (NODE* root);

/*	================= AVL_Create ================
	Allocates dynamic memory for an BST tree head
	node and returns its address to caller
*/
AVL_TREE* AVL_Create(int  (*compare) (void* argu1, void* argu2))
{
	AVL_TREE* tree;

	tree = (AVL_TREE*) malloc (sizeof (AVL_TREE));
	if (tree){
	    tree->root    = NULL;
	    tree->count   = 0;
	    tree->compare = compare;
	}

	return tree;
}

/*	================= AVL_Insert ===================
	This function inserts new data into the tree.
*/
bool AVL_Insert (AVL_TREE* tree, void* dataPtr)
{
	NODE* newPtr;

	newPtr = (NODE*)malloc(sizeof(NODE));
	if (!newPtr)
	   return false;

	newPtr->right   = NULL;
	newPtr->left    = NULL;
	newPtr->dataPtr = dataPtr;

	if (tree->count == 0) {
	    tree->root  =  newPtr;
        newPtr->height = 1;
	}
	else {
	    _insert(tree, tree->root, newPtr);
        heightFix(newPtr);
        newPtr = balance(newPtr);
	}

	(tree->count)++;
	return true;
}

static NODE* _insert (AVL_TREE* tree, NODE* root, NODE* newPtr)
{
	if (!root) // if NULL tree
	   return newPtr;

	// Locate null subtree for insertion
	if (tree->compare(newPtr->dataPtr,
	                  root->dataPtr) < 0){
	    root->left = _insert(tree, root->left, newPtr);
	    return root;
	}     // new < node
	else { // new data >= root data
	    root->right = _insert(tree, root->right, newPtr);
	    return root;
	} // else new data >= root data

	return root;
}

/* ================== AVL_Delete ==================
	This function deletes a node from the tree and
	rebalances it if necessary.
*/
bool AVL_Delete (AVL_TREE* tree, void* dltKey)
{
	bool  success;
	NODE* newRoot;

	newRoot = _delete (tree, tree->root, dltKey, &success);
	heightFix(newRoot);
	newRoot = balance(newRoot);

	if (success){
	    tree->root = newRoot;
	    (tree->count)--;
	    if (tree->count == 0) // Tree now empty
	        tree->root = NULL;
	}
	return success;
}

static NODE*  _delete (AVL_TREE* tree, NODE* root, void* dataPtr, bool* success)
{
	NODE* dltPtr;
	NODE* exchPtr;
	NODE* newRoot;
	void* holdPtr;

	if (root == NULL){
	    *success = false;
	    return NULL;
	}

	if ((tree->compare(dataPtr, root->dataPtr)) < 0) //if the given dataPtr comes before root, check left subtree
	    root->left  = _delete (tree, root->left, dataPtr, success);
	else if ((tree->compare(dataPtr, root->dataPtr)) > 0) //if given dataPtr comes after root, check right subtree
	    root->right = _delete (tree, root->right, dataPtr, success);
	else { // Delete node found--test for leaf node
	    dltPtr = root;
		if (!root->left){         // Only right subtree
            free (root->dataPtr);       // data memory
	        newRoot = root->right;
	        free (dltPtr);              // BST Node
	        *success = true;
	        return newRoot;             // base case
	     }
	     else
			 if (!root->right){   // Only left subtree
	             newRoot = root->left;
	             free (dltPtr);
	             *success = true;
	             return newRoot;         // base case
	         }
			 else{ // Delete Node has two subtrees
                 exchPtr = root->left;
	             // Find largest node on left subtree
	             while (exchPtr->right)
	                 exchPtr = exchPtr->right;

	              // Exchange Data
	              holdPtr          = root->dataPtr;
	              root->dataPtr    = exchPtr->dataPtr;
	              exchPtr->dataPtr = holdPtr;
	              root->left       =
	                 _delete (tree,   root->left,
	                          exchPtr->dataPtr, success);
			 }
	}
	return root;
}

/*	==================== AVL_Retrieve ===================
	Retrieve node searches tree for the node containing
	the requested key and returns pointer to its data.
*/
void* AVL_Retrieve  (AVL_TREE* tree, void* dataPtr)
{
	if (tree->root)
	    return _retrieve (tree, dataPtr, tree->root);
    return NULL;
}

static void* _retrieve (AVL_TREE* tree, void* dataPtr, NODE* root)
{
	if (root){
	     if (tree->compare(dataPtr, root->dataPtr) < 0)
	         return _retrieve(tree, dataPtr, root->left);
	     else if (tree->compare(dataPtr, root->dataPtr) > 0)
	         return _retrieve(tree, dataPtr, root->right);
	     else
	         // Found equal key
	         return root->dataPtr;
	}
	else

	    return NULL;
}

/*	=============== AVL_Destroy ==============
	Deletes all data in tree and recycles memory.
	The nodes are deleted by calling a recursive
	function to traverse the tree in inorder sequence.
*/
AVL_TREE* AVL_Destroy (AVL_TREE* tree)
{
	if (tree)
		_destroy (tree->root);

	// All nodes deleted. Free structure
	free (tree);
	return NULL;
}

static void _destroy (NODE* root)
{
	if (root){
	    _destroy (root->left);
	    free (root->dataPtr);
	    _destroy (root->right);
	    free (root);
	}
	return;
}

NODE* rotateRight(NODE* node) {

    NODE* temp1 = node->left;
    NODE* temp2 = temp1->right;

    temp1->right = node;
    node->left = temp2;

    heightFix(node);
    heightFix(temp1);

    return temp1;

}

NODE* rotateLeft(NODE* node) {

    NODE* temp1 = node->right;
    NODE* temp2 = temp1->left;

    temp1->left = node;
    node->right = temp2;

    heightFix(node);
    heightFix(temp1);

    return temp1;
}

void heightFix(NODE* node) {

    if((node->right == NULL) && (node->left == NULL)) {
        node->height = 0; //set height to what node with no children should be?
    }
    else if((node->right) && (node->left == NULL)) {
        node->height = node->right->height + 1;
    }
    else if((node->right == NULL) && (node->left)) {
        node->height = node->left->height + 1;
    }
    else {
        node->height = max(node->left->height, node->right->height) + 1;
    }
}

int heightBalance(NODE* node) {

    if(node->right && node->left) {
        return (node->right->height - node->left->height);
    }

    if(node->left)
        return (0 - node->left->height);
    if(node->right)
        return node->right->height;

    return 0;
}

int max(int a, int b) {

    if(a > b) {
        return a;
    }
    if(b > a) {
        return b;
    }
    return a;
}

NODE* balance(NODE* node) {

    //left heavy
    if(heightBalance(node) < 0) {
        //Left Left
        if(heightBalance(node) < -1) {
            node = rotateRight(node);
        //Left Right
        } else {
            node = rotateLeft(node);
            node = rotateRight(node);
        }
    }
    //right heavy
    else if(heightBalance(node) > 0) {
        //Right Right
        if(heightBalance(node) > 1) {
            node = rotateLeft(node);
        //Right Left
        } else {
            node = rotateRight(node);
            node = rotateLeft(node);
        }
    }

    return node;
}


