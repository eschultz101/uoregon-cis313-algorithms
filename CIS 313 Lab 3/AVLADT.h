
//Eric Schultz
//11/7/14
//Header file that includes all prototypes for AVL tree functions

typedef enum {false, true} bool;

typedef struct node
	{
	 void*        dataPtr;
	 struct node* left;
	 struct node* right;
	 int height;
	} NODE;

typedef struct
	{
	 int   count;
	 int  (*compare) (void* argu1, void* argu2);
	 NODE*  root;
	} AVL_TREE;

AVL_TREE* AVL_Create(int (*compare) (void* argu1, void* argu2));
AVL_TREE* AVL_Destroy (AVL_TREE* tree);
bool  AVL_Insert   (AVL_TREE* tree, void* dataPtr);
bool  AVL_Delete   (AVL_TREE* tree, void* dltKey);
void* AVL_Retrieve (AVL_TREE* tree, void* keyPtr);
NODE* rotateRight(NODE* node);
NODE* rotateLeft(NODE* node);
void heightFix(NODE* node);
int heightBalance(NODE* node);
int max(int a, int b);
NODE* balance(NODE* node);

