//Eric Schultz
//CIS 313 Lab 3
//12/2/2014

#include "AVLADT.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX_OPERATIONS 20
#define MAX_CHARS 100

int compare(void*, void*);

int main(void) {

    char results[MAX_OPERATIONS][MAX_CHARS] = { {'\0'} };
    char book_titles[MAX_OPERATIONS][MAX_CHARS] = { { '\0' } };
    int num_ops = 0, i = 0;
    FILE* fpIn, *fpOut;

    AVL_TREE* avlTree = AVL_Create(compare);

    if((fpIn = fopen("input.txt", "r")) == NULL) {
        printf("Input file failed to open.\n");
        return -1;
    }

    fscanf(fpIn, "%d%*c", &num_ops);

    for(i = 0; i < num_ops; i++) {

        char operation[MAX_CHARS];
        char* temp = NULL;
        bool flag = false;

        fscanf(fpIn, "%s%*c", operation);
        fscanf(fpIn, "%[^\n]", book_titles[i]);

        if(strcmp(operation, "insert") == 0) {
            flag = AVL_Insert(avlTree, book_titles[i]);
            if(flag) {
                strcpy(results[i], "Inserted ");
                strcat(results[i], book_titles[i]);
                strcat(results[i], " into the catalog.");
            } else {
                strcpy(results[i], book_titles[i]);
                strcat(results[i], " has not been added.");
            }
            printf("Root is currently %s\n", (char*)(avlTree->root->dataPtr));
        }
        else if(strcmp(operation, "find") == 0) {
            temp = AVL_Retrieve(avlTree, book_titles[i]);
            if(temp) {
                strcat(results[i], book_titles[i]);
                strcat(results[i], " is in the catalog.");
            } else {
                strcpy(results[i], book_titles[i]);
                strcat(results[i], " is not in the catalog.");
            }
        }
        else if(strcmp(operation, "remove") == 0) {
            flag = AVL_Delete(avlTree, book_titles[i]);
            if(flag == true) {
                strcpy(results[i], book_titles[i]);
                strcat(results[i], " was removed from the catalog.");
            } else {
                strcpy(results[i], book_titles[i]);
                strcat(results[i], " is not in the catalog.");
            }
        }
        else {
            printf("Invalid operation entered.\n");
            return -1;
        }

    }

    if((fpOut = fopen("output.txt", "w")) == NULL) {
        printf("Output file failed to open.\n");
        return -1;
    }

    for(i = 0; i < num_ops; i++) {
        fprintf(fpOut, "%s\n", results[i]);
    }

    fclose(fpIn);
    fclose(fpOut);

    AVL_Destroy(avlTree);

    return 0;
}

int compare(void* arg1, void* arg2) {

    return strcmp(arg1, arg2);
}
