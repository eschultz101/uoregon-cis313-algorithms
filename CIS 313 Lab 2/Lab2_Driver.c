
//Eric Schultz
//11/7/14
//CIS 313 Lab 2
//Books with Binary Search Tree

#include "BSTADT.h"
#include <stdio.h>
#include <string.h>

#define MAX_OPERATIONS 20
#define MAX_CHARS 100

int compare(void*, void*);

int main(void) {

    char results[MAX_OPERATIONS][MAX_CHARS] = { {'\0'} };
    char book_titles[MAX_OPERATIONS][MAX_CHARS] = { { '\0' } };
    int num_ops = 0, i = 0;

    BST_TREE* tree = BST_Create(compare);
    FILE* fpIn, *fpOut;

    if((fpIn = fopen("input.txt", "r")) == NULL) {
        printf("Input file failed to open.\n");
        return -1;
    }

    fscanf(fpIn, "%d%*c", &num_ops);

    for(i = 0; i < num_ops; i++) {

        char operation[MAX_CHARS];
        char* temp = NULL;
        bool flag = false;

        fscanf(fpIn, "%s%*c", operation);
        fscanf(fpIn, "%[^\n]", book_titles[i]);

        if(strcmp(operation, "insert") == 0) {
            flag = BST_Insert(tree, book_titles[i]);
            if(flag) {
                strcpy(results[i], "Inserted ");
                strcat(results[i], book_titles[i]);
                strcat(results[i], " into the catalog.");
            } else {
                strcpy(results[i], book_titles[i]);
                strcat(results[i], " has not been added.");
            }
        }
        else if(strcmp(operation, "find") == 0) {
            temp = BST_Retrieve(tree, book_titles[i]);
            if(temp) {
                strcat(results[i], book_titles[i]);
                strcat(results[i], " is in the catalog.");
            } else {
                strcpy(results[i], book_titles[i]);
                strcat(results[i], " is not in the catalog.");
            }
        }
        else if(strcmp(operation, "remove") == 0) {
            flag = BST_Delete(tree, book_titles[i]);
            if(flag == true) {
                strcpy(results[i], book_titles[i]);
                strcat(results[i], " was removed from the catalog.");
            } else {
                strcpy(results[i], book_titles[i]);
                strcat(results[i], " is not in the catalog.");
            }
        }
        else {
            printf("Invalid operation entered.\n");
            return -1;
        }

    }

    if((fpOut = fopen("output.txt", "w")) == NULL) {
        printf("Output file failed to open.\n");
        return -1;
    }

    for(i = 0; i < num_ops; i++) {
        fprintf(fpOut, "%s\n", results[i]);
    }

    fclose(fpIn);
    fclose(fpOut);

    BST_Destroy(tree);

    return 0;
}

/* ================ compare =================
    Compares two pieces of data (in this case
    strings) to determine whether or not to
    place a node to the right or left of the
    root.
*/
int compare(void* arg1, void* arg2){

    int res;
    res = strcmp(arg1, arg2);

    if(res > 0) {
        return 1;
    } else if(res < 0) {
        return -1;
    }

    return 0;

}
