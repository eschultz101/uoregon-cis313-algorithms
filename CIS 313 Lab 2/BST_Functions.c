
//Eric Schultz
//11/7/2014
//File with functions for binary search tree

#include <ctype.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "BSTADT.h"

//BST: Prototype Declarations for private functions
static NODE* _insert (BST_TREE* tree,
					  NODE* root,
					  NODE* newPtr);
static NODE* _delete (BST_TREE* tree,
					  NODE* root,
					  void* dataPtr,
					  bool* success);
static void* _retrieve(BST_TREE* tree,
					   void* dataPtr,
					   NODE* root);
static void _destroy (NODE* root);

/*	================= BST_Create ================
	Allocates dynamic memory for an BST tree head
	node and returns its address to caller
*/
BST_TREE* BST_Create(int  (*compare) (void* argu1, void* argu2))
{
	BST_TREE* tree;

	tree = (BST_TREE*) malloc (sizeof (BST_TREE));
	if (tree){
	    tree->root    = NULL;
	    tree->count   = 0;
	    tree->compare = compare;
	}

	return tree;
}

/*	================= BST_Insert ===================
	This function inserts new data into the tree.
*/
bool BST_Insert (BST_TREE* tree, void* dataPtr)
{
	NODE* newPtr;

	newPtr = (NODE*)malloc(sizeof(NODE));
	if (!newPtr)
	   return false;

	newPtr->right   = NULL;
	newPtr->left    = NULL;
	newPtr->dataPtr = dataPtr;

	if (tree->count == 0)
	    tree->root  =  newPtr;
	else
	    _insert(tree, tree->root, newPtr);

	(tree->count)++;
	return true;
}

static NODE* _insert (BST_TREE* tree, NODE* root, NODE* newPtr)
{
	if (!root) // if NULL tree
	   return newPtr;

	// Locate null subtree for insertion
	if (tree->compare(newPtr->dataPtr,
	                  root->dataPtr) < 0){
	    root->left = _insert(tree, root->left, newPtr);
	    return root;
	}     // new < node
	else{ // new data >= root data
	    root->right = _insert(tree, root->right, newPtr);
	    return root;
	} // else new data >= root data
	return root;
}

/* ================== BST_Delete ==================
	This function deletes a node from the tree and
	rebalances it if necessary.
*/
bool BST_Delete (BST_TREE* tree, void* dltKey)
{
	bool  success;
	NODE* newRoot;

	newRoot = _delete (tree, tree->root, dltKey, &success);
	if (success){
	    tree->root = newRoot;
	    (tree->count)--;
	    if (tree->count == 0) // Tree now empty
	        tree->root = NULL;
	}
	return success;
}

static NODE*  _delete (BST_TREE* tree, NODE* root, void* dataPtr, bool* success)
{
	NODE* dltPtr;
	NODE* exchPtr;
	NODE* newRoot;
	void* holdPtr;

	if (root == NULL){
	    *success = false;
	    return NULL;
	}

	if ((tree->compare(dataPtr, root->dataPtr)) < 0) //if the given dataPtr comes before root, check left subtree
	    root->left  = _delete (tree, root->left, dataPtr, success);
	else if ((tree->compare(dataPtr, root->dataPtr)) > 0) //if given dataPtr comes after root, check right subtree
	    root->right = _delete (tree, root->right, dataPtr, success);
	else { // Delete node found--test for leaf node
	    dltPtr = root;
		if (!root->left){         // Only right subtree
            free (root->dataPtr);       // data memory
	        newRoot = root->right;
	        free (dltPtr);              // BST Node
	        *success = true;
	        return newRoot;             // base case
	     }
	     else
			 if (!root->right){   // Only left subtree
	             newRoot = root->left;
	             free (dltPtr);
	             *success = true;
	             return newRoot;         // base case
	         }
			 else{ // Delete Node has two subtrees
                 exchPtr = root->left;
	             // Find largest node on left subtree
	             while (exchPtr->right)
	                 exchPtr = exchPtr->right;

	              // Exchange Data
	              holdPtr          = root->dataPtr;
	              root->dataPtr    = exchPtr->dataPtr;
	              exchPtr->dataPtr = holdPtr;
	              root->left       =
	                 _delete (tree,   root->left,
	                          exchPtr->dataPtr, success);
			 }
	}
	return root;
}

/*	==================== BST_Retrieve ===================
	Retrieve node searches tree for the node containing
	the requested key and returns pointer to its data.
*/
void* BST_Retrieve  (BST_TREE* tree, void* dataPtr)
{
	if (tree->root)
	    return _retrieve (tree, dataPtr, tree->root);
    return NULL;
}

static void* _retrieve (BST_TREE* tree, void* dataPtr, NODE* root)
{
	if (root){
	     if (tree->compare(dataPtr, root->dataPtr) < 0)
	         return _retrieve(tree, dataPtr, root->left);
	     else if (tree->compare(dataPtr, root->dataPtr) > 0)
	         return _retrieve(tree, dataPtr, root->right);
	     else
	         // Found equal key
	         return root->dataPtr;
	}
	else

	    return NULL;
}

/*	=============== BST_Destroy ==============
	Deletes all data in tree and recycles memory.
	The nodes are deleted by calling a recursive
	function to traverse the tree in inorder sequence.
*/
BST_TREE* BST_Destroy (BST_TREE* tree)
{
	if (tree)
		_destroy (tree->root);

	// All nodes deleted. Free structure
	free (tree);
	return NULL;
}

static void _destroy (NODE* root)
{
	if (root){
	    _destroy (root->left);
	    free (root->dataPtr);
	    _destroy (root->right);
	    free (root);
	}
	return;
}
