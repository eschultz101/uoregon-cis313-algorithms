
//Eric Schultz
//11/7/14
//Header file that includes all prototypes for binary search tree functions

typedef enum {false, true} bool;

//BST type definitions
typedef struct node
	{
	 void*        dataPtr;
	 struct node* left;
	 struct node* right;
	} NODE;

typedef struct
	{
	 int   count;
	 int  (*compare) (void* argu1, void* argu2);
	 NODE*  root;
	} BST_TREE;

BST_TREE* BST_Create(int (*compare) (void* argu1, void* argu2));
BST_TREE* BST_Destroy (BST_TREE* tree);
bool  BST_Insert   (BST_TREE* tree, void* dataPtr);
bool  BST_Delete   (BST_TREE* tree, void* dltKey);
void* BST_Retrieve (BST_TREE* tree, void* keyPtr);


